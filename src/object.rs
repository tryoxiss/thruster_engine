// probablyu obsolted by the signal system

// pub trait Update
// {
	/**
	A function that is run as fast as possible. Note that this function is
	expected to be async, as its execution may be halted at any time to
	run the fixed update instead if the interval has passed.
	*/
	// #[allow(unused_variables)]
	// async fn update(&self, delta: f32)
	// {
		// Intentionally blank so it does not need to be
		// implemented if not needed or desired.
	// }

	/**
	A fixed update for things that need to happen before each physics step.
	This is very simillar to `fixed_update()` only that this will always
	complete *before* physics are processed, but the regular fixed update may
	complete at any point during the update.
	*/
	// #[allow(unused_variables)]
	// fn physics_update(&self, delta: f32)
	// {
		// Intentionally blank so it does not need to be
		// implemented if not needed or desired.
	// }

	/**
	This is called once for every game loop update, by default 60 times
	per seccond. This should be used to handle things that need to happen
	exactly once per update, but may happen at any time during that update.

	Note that while this update is "fixed", as in normally it will be
	called exactly `TICKRATE` every seccond, you should still multiply
	by delta as if it were in update as if the hardware is unable to keep
	up the fixed update will slow down.
	*/
	// #[allow(unused_variables)]
	// async fn fixed_update(&self, delta: f32);
	
		// Intentionally blank so it does not need to be
		// implemented if not needed or desired.
	
// }

pub trait Ready
{
	/**
	Run once when the object is instanced.
	*/
	fn ready(&mut self)
	{

	}
}

pub trait Node: Update + Ready {}

#[allow()]
pub trait Resource: Ready + Drop + Send + Sync {}