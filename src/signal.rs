pub trait Signal
{

}

pub trait Emit<S: Signal>
{
	fn prepare(&self);

	fn emit(&self)
	{
		// pretend return sends it to the engine
		return self.prepare();
	}
}

pub trait Recieve<S: Signal>
{
	fn recieve(&self, signal: S);
}

pub trait RecieveMut<S: Signal>
{
	fn recieve(&mut self, signal: S);
}

/// demo stuff

pub struct IsHungry
{
	hunger: u8
}

pub struct WantsCuddles;

pub struct Cat
{
	is_hungry: bool
}

impl Emit<IsHungry> for Cat
{
	fn prepare(&self)
	{
		return IsHungry { hunger: 4 };
	}
}

impl Emit<WantsCuddles> for Cat {}


fn a()
{
	let cat = Cat::new();

	cat.emit::<IsHungry>();
}





impl Recieve<WantsCuddles> for Dog
{
	fn recieve(&self, signal: WantsCuddles)
	{
		signal.entity.snuggle();
	}
}

impl Recieve<FixedUpdate> for Dog
{
	fn recieve(&self, signal: FixedUpdate)
	{
		// do stuff
		let x = signal.delta;
	}
}